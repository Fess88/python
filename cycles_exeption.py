#!/usr/bin/env python3

print("Введите число и нажмите Enter, или просто Enter, чтобы завершить")

total = 0 # сумма всех введенных данных
count = 0 # счетчик только введенных чисел

while True:
    line = input("integer: ")
    if line:
        try: # проверяем условие
            number = int(line) # если без ошибок, то блок Except пропуск
        except ValueError as err: # иначе
            print(err) # выводим сообщение об ошибке
            continue # и продолжаем цикл
        total += number
        count += 1
    else:
        break  # Если ничего не ввели, завершаем цикл

if count:
    print("count = ", count, "total = ", total, "mean =", total/count)
